package br.com.easytax.receiver.service.rule;

import br.com.easytax.iamlib.exception.BadRequestException;
import br.com.easytax.receiver.domain.TaxParam;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class RuleServiceAPI {

    public static final String SOMETHING_WRONG_HAPPENED = "Something wrong happened. %s";
    @Value("${services.rule.endpoint}")
    private String baseURL;

    public Optional<Boolean> validateParams(final TaxParam taxParam) {

        try {
            final WebClient client = WebClient.builder()
                    .baseUrl(baseURL)
                    .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .defaultUriVariables(Collections.singletonMap("url", baseURL))
                    .build();

            return Optional.ofNullable(
                    client.method(HttpMethod.POST)
                            .uri("/api/rule/validate-params")
                            .bodyValue(taxParam)
                            .retrieve()
                            .bodyToMono(Boolean.class)
                            .block()
            );
        } catch (WebClientResponseException e) {
            if (e.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {

                final ObjectMapper objectMapper = new ObjectMapper();
                try {
                    List<String> messages = objectMapper.readValue(e.getResponseBodyAsString(), List.class);
                    throw new BadRequestException(messages.stream().collect(Collectors.joining(". ")));
                } catch (JsonProcessingException ex) {
                    throw new BadRequestException(String.format(SOMETHING_WRONG_HAPPENED, e.getMessage()));
                }
            }
            throw new BadRequestException(String.format(SOMETHING_WRONG_HAPPENED, e.getMessage()));
        } catch (Exception e) {
            throw new BadRequestException(String.format(SOMETHING_WRONG_HAPPENED, e.getMessage()));
        }

    }

}
