package br.com.easytax.receiver.service;

import br.com.easytax.iamlib.exception.BadRequestException;
import br.com.easytax.receiver.domain.TaxParam;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class RequestTaxCalculationService {

    static final String topicExchangeName = "spring-boot-exchange";
    private final RabbitTemplate rabbitTemplate;
    private final ObjectMapper objectMapper;

    public void send(final TaxParam taxParam) {
        sendMessage(taxParam);
    }

    private void sendMessage(final TaxParam taxParam) {
        try {
            rabbitTemplate.convertAndSend(topicExchangeName, "br.com.easytax", objectMapper.writeValueAsString(taxParam));
        } catch (JsonProcessingException e) {
            throw new BadRequestException(e.getMessage());
        }
    }

}
