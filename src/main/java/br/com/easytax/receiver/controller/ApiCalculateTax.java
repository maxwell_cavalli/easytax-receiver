package br.com.easytax.receiver.controller;

import br.com.easytax.receiver.domain.TaxParam;
import br.com.easytax.receiver.service.RequestTaxCalculationService;
import br.com.easytax.iamlib.security.Privilege;
import br.com.easytax.iamlib.security.SecuredContext;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@AllArgsConstructor
@Slf4j
@RestController
@RequestMapping("/api/calculate-tax")
@SecuredContext(context = "CalculateTax")
public class ApiCalculateTax {

    private final RequestTaxCalculationService requestTaxCalculationService;

    @Privilege(actions = {"post"})
    @PostMapping
    public ResponseEntity<String> calculateTax(@Valid @RequestBody final TaxParam taxParam) {
        log.info(taxParam.toString());
        requestTaxCalculationService.send(taxParam);

        return ResponseEntity.ok(taxParam.getRequestId());
    }

}
