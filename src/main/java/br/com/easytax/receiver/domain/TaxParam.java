package br.com.easytax.receiver.domain;

import lombok.Data;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@ToString
@Data
public class TaxParam {
    private String requestId;

    @NotBlank(message = "is required")
    private String city;

    @NotBlank(message = "is required")
    private String serviceCode;

    @NotNull(message = "is required")
    private Date documentDate;

    @NotNull(message = "is required")
    @Valid
    private List<TaxParamDetail> details;

    private String userId;

    private String customerId;

    public TaxParam() {
        this.requestId = UUID.randomUUID().toString();
    }
}
