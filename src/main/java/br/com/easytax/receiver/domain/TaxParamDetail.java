package br.com.easytax.receiver.domain;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

@ToString
@Data
public class TaxParamDetail {

    @NotEmpty(message = "is required")
    private String name;

    private String stringValue;
    private Double doubleValue;
    private Integer integerValue;

}
